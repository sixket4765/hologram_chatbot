# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup
import json

from flask import Flask, request
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import sys


class WebClass:
    def __init__(self):
        options = webdriver.ChromeOptions()
        #options.add_argument('headless')
        options.add_argument('window-size=1920x1080')
        options.add_argument("disable-gpu")
        options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
        self.driver = webdriver.Chrome('chromedriver', chrome_options=options)

class Game:
    def __init__(self):
        self.isGame = False
        self.front_url = 'https://ko.dict.naver.com/#/search?range=entry&page='
        self.end_url = '&query=' # +word[-1]+'*' 해야됨'
        self.dictionary = {}
        self.numberList = '0123456789-'
        self.gameList = []
        self.driver = WebClass().driver
        self.recentWord = ''
        self.last = ''
        self.sourceList = ['표준국어대사전','Standard Korean Dict', '우리말샘', '고려대 한국어대사전']
        #이제까지 게임이 진행된 단어순으로 저장.
    
    def gameStart(self):
        self.last = ''
        self.isGame = True
        #self.driver.quit()
        #self.driver = WebClass().driver
        #이전 게임들의 대한 정보를 초기화
        self.gameList = []
        self.recentWord = ''
        for key in self.dictionary.keys():
            self.dictionary[key]['index'] = 0
            self.dictionary[key]['page'] = 1
            self.dictionary[key]['wordList'] = []
            self.dictionary[key]['comment'] = []
        self.last = ''
        #인덱스 초기화

    def gameEnd(self):
        self.isGame = False
        #그냥 게임 정보만 False로 한다.
        #초기화는 게임 시작시에
    
    def getGameState(self):
        return self.isGame

    def isRightWord(self ,word: str, tag, wordList: list):
        #크롤링할때 찾은 단어가 올바른 단어인지 확인
        #단어가 표준사전에 있고 안나온 단어이면 리스트에 저장.
        if word.startswith('-'):
            print('-')
            return False
        if tag.find('p', class_='source') is None:
            print('소스없음')
            return False
        if ' ' in word:
            return False
        if word in wordList:
            return False
        source = tag.find('p', class_='source')
        #출처가 어디에 있는지 확인함.
        if source is None :
            return False
        source = source.get_text().strip()
        #print('소스 - ',source, file=sys.stdout)
        if source in self.sourceList:
            return True
        return False
              
    def getWord(self, word :str):
        #word는 사용자의 단어
        self.last = ''
        last = word[-1]
        if not (last in self.dictionary) :#self.dictionary[last] is None:
            #처음으로 단어를 봤을때
            self.driver.get(self.front_url + '1' + self.end_url + last + '*')
            #'https://ko.dict.naver.com/#/search?range=entry&page=2&query=다*'가 됨
            print(self.front_url + '1' + self.end_url + last + '*', file=sys.stdout)
            # seconds
            html = ''
            try:
                WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.ID, 'searchPage_entry_paginate'))
                )
            except :
                print('error')
            finally:
                html = self.driver.page_source
            
            #content(메인)이 다 완료될때까지 기다리는 중

            #with open('zzz.html', 'w') as file:
            #    file.write(html)

            #print(html, file=sys.stdout)
            soup = BeautifulSoup(html, 'html.parser')
            
            self.dictionary[last] = {}
            self.dictionary[last]['page'] = 1
            self.dictionary[last]['wordList'] = []
            self.dictionary[last]['index'] = 0
            self.dictionary[last]['comment'] = []
            #새로운 단어 추가할때 초기화 부분.

            for findedWord in soup.find_all('div', class_='row'):
                tmp = findedWord.find('a', class_='link').get_text()
                for number in self.numberList:
                    tmp = tmp.replace(number, '')
                tmp = tmp.strip()
                #print(tmp, file=sys.stdout)
                if len(tmp) > 1:
                    #print('new', file=sys.stdout)
                    #단어의 적합성을 판단한다.
                    if self.isRightWord(tmp, findedWord ,self.dictionary[last]['wordList']):#findedWord는 soup의 태그
                        self.dictionary[last]['wordList'].append(tmp)
                        self.dictionary[last]['comment'].append(findedWord.find('p', class_='mean').get_text().strip())
                        #self.dictionary[last]
                        #print(tmp)
        
        print(self.dictionary[last]['wordList'], sys.stdout)
        if len(self.dictionary[last]['wordList']) == 0:
            #아무것도 찾을 수가 없을때
            return u'You win', 1

        #for passedWord in self.gameList:
        print(self.dictionary[last]['wordList'], file=sys.stdout)


        while True:
            #page 넘어가면서 새롭게 갱신
            index = self.dictionary[last]['index']
            for nextWord in self.dictionary[last]['wordList'][index:]:
                self.dictionary[last]['index'] += 1 # = (idx + 1) + index
                if not(nextWord in self.gameList): 
                    #이전 단어에 없는경우.
                    print(nextWord, file=sys.stdout)
                    self.last = last
                    return nextWord, 0# + '\n' + self.dictionary[last]['comment'][self.dictionary[last]['index'] - 1], 0
            print('while ', file=sys.stdout)
            if self.dictionary[last]['index'] >= len(self.dictionary[last]['wordList']) :
                self.dictionary[last]['page'] += 1
                self.driver.get(self.front_url + str(self.dictionary[last]['page']) + self.end_url + last + '*')
                print(self.front_url + '1' + self.end_url + last + '*', file=sys.stdout)
                # seconds

                html = ''
                try:
                    WebDriverWait(self.driver, 10).until(
                        EC.presence_of_element_located((By.ID, 'searchPage_entry_paginate'))
                    )
                except :
                    print('error')
                finally:
                    html = self.driver.page_source

                soup = BeautifulSoup(html, 'html.parser')
                test = soup.find('div', class_='paginate')
                if int(test.find('strong').get_text()) < self.dictionary[last]['page']:
                    #page가 끝을 넘어갈 경우 지는것.
                    return u'You Win', 1

                for findedWord in soup.find_all('div', class_='row'):
                    tmp = findedWord.find('a', class_='link').get_text()
                    for number in self.numberList:
                        tmp = tmp.replace(number, '')
                    tmp = tmp.strip()
                    #print(tmp, file=sys.stdout)
                    if len(tmp) > 1:
                        #단어의 적합성을 판단한다.
                        if self.isRightWord(tmp, findedWord ,self.dictionary[last]['wordList']):#findedWord는 soup의 태그
                            self.dictionary[last]['wordList'].append(tmp)
                            self.dictionary[last]['comment'].append(findedWord.find('p', class_='mean').get_text().strip())
                            #print(tmp)
                


        #위의 while을 절때 벗어날 수가 없다. 전부 return을 하니.
        return word, 1



SLACK_TOKEN = 'xoxb-682997044292-678235554595-ESTBkEME9h2ViDbswFQ3mXZV'
SLACK_SIGNING_SECRET = '82f609920344b92337d27d2ea71086bc'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
game = Game()

def gameManager(text):
    textList = text.replace('>', ' ').split()
    word = textList[1]
    print(word , file=sys.stdout)
    if word == 'help':
        return u'gamestart, gameend'

    elif word == 'gamestart':
        if game.getGameState():
            return u'게임이 진행중 입니다.\n 계속하시거나 먼저 게임을 종료하세요'
        game.gameStart()
        return u'게임이 시작되었습니다.'
    
    elif word == 'gameend':
        if game.getGameState() == False:
            return u'게임을 먼저 시작하세요'
        game.gameEnd()
        return u'게임이 종료되었습니다.'
    
    if game.getGameState():
        #게임이 진행되는 중
        #영어나 숫자가 있어서는 안된다.
        print('game start', file=sys.stdout)
        result = re.search("[0-9a-zA-Z]", word)
        if result is not None:
            return u'숫자나 영어가 있어서는 안됩니다.'
        if (len(game.recentWord) != 0) and (not(word.startswith(game.recentWord[-1]))):
            return game.recentWord[-1] + '로 시작되야 합니다.'

        if len(word) <= 1:
            return u'2글자 이상이여야 됩니다.'
        if word in game.gameList:
            return u'이전에 있던 단어 입니다.'
        

        print(game.driver, file=sys.stdout)
        #recentword에는 적합한 최근 단어만 저장하기 getword에 return을 2개로 함.
        '''
        tmpStr = game.recentWord
        game.gameList.append(word)
        #게임 정보 저장
        game.recentWord, resultInt = game.getWord(word)
        if resultInt == 0:
            #0이면 정상적으로 단어 출력
            game.gameList.append(game.recentWord)
            #게임 정보 저장
            return game.recentWord
        elif resultInt == 1:
            returnStr = game.recentWord
            game.recentWord = tmpStr
            #1이면 사용자가 이긴것.
            return returnStr
        '''
        
        #뭔가 이상한 에러 발생
        
        game.driver.get(game.front_url + '1' + game.end_url + word)
        print(game.front_url + '1' + game.end_url + word, file=sys.stdout)
        html = ''
        try:
            WebDriverWait(game.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'row'))
            )
        except :
            print('error')
        finally:
            html = game.driver.page_source

        soup2 = BeautifulSoup(html, 'html.parser')

        wordTag = soup2.find('div', class_='row')
        if wordTag is None:
            return u'단어가 없습니다.'
        
        tmp = wordTag.find('a', class_='link').get_text()
        for number in game.numberList:
            tmp = tmp.replace(number, '')
        tmp = tmp.strip()
        if tmp != word:
            print('wrong error ',tmp, word, file=sys.stdout)
            return u'단어가 없습니다'

        #print(tmp, file=sys.stdout)
        #단어의 적합성을 판단한다.
        if game.isRightWord(tmp, wordTag , game.gameList):#findedWord는 soup의 태그
            #사용자의 마지막말을 처음으로 하는 단어를 찾는다.
            print('찾기', file=sys.stdout)
            tmpStr = game.recentWord
            game.gameList.append(word)
            #게임 정보 저장
            game.recentWord, resultInt = game.getWord(word)
            print(game.recentWord, resultInt, '이상', file=sys.stdout)
            if resultInt == 0:
                #0이면 정상적으로 단어 출력
                game.gameList.append(game.recentWord)
                #게임 정보 저장
                #return은 해당단어와 그 코멘트 출력
                return game.recentWord
            elif resultInt == 1:
                game.last = ''
                returnStr = game.recentWord
                game.recentWord = tmpStr
                #1이면 사용자가 이긴것.
                return returnStr
            
            #print(tmp)
        return u'적합한 단어가 아닙니다, 출처가 이상합니다'
        

    else :
        return u'잘못된 명령어 입니다.'

# 크롤링 함수 구현하기
def _crawl_music_chart(text):

    textList = text.split()
    word = textList[1]
    print(word)
    print(word, file=sys.stdout)
    print('consol', file=sys.stdout)
    
    print(game.getGameState(), file=sys.stdout)
    if 'music' in text :
        top = 10
        url = 'https://music.bugs.co.kr/chart?wl_ref=M_left_02_01'
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        message = []
        i = 1

        for data in soup.find_all('p', class_='title'):
            message.append("%d위 : "%(i) + data.get_text().strip())
            i += 1
            if i >= top + 1:
                break

        i = -1
        listTable = soup.find('table', class_='list trackList byChart')
        for art in listTable.find_all('p', class_='artist'):
            i += 1
            if i >= top:
                break
            if art.find('a', class_='artistTitle') is not None:
                message[i] += '/ ' + art.find('a', class_='artistTitle').get_text().strip()
                continue
            message[i] += '/ ' + art.get_text().strip()


        return u'\n'.join(message)

    
    elif 'movie' in text:
        url = 'https://movie.naver.com/movie/sdb/rank/rmovie.nhn?sel=cnt&date=20190709'
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        message = []
        i = 1
        for data in (soup.find_all('div', class_='tit3')):
            message.append("%d위 : "%(i) + data.get_text().strip())
            i += 1

        return u'\n'.join(message)
    else :
        return "`@<봇이름> music or movie` 과 같이 멘션해주세요."

    # 여기에 함수를 구현해봅시다.

prev_client_msg_id = {}

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    message = '*'+gameManager(text)+'*'#_crawl_music_chart(text)
    client_msg_id = event_data['event']['client_msg_id']
    print(client_msg_id, file=sys.stdout)
    if client_msg_id in prev_client_msg_id:
        return
    else:
        prev_client_msg_id[client_msg_id] = 1

    button_actions = ActionsBlock(
        block_id='game_button',
        elements=[
            ButtonElement(
                text="Game Reset",
                action_id="game_reset", value = str(1)
            ),
            ButtonElement(
                text="Game End",
                action_id="game_end", value = str(2)
            ),
        ]
    )
    if len(game.last) == 0:    
        messageBlocks = [SectionBlock(text=message), button_actions]
    else:
        idx = game.dictionary[game.last]['index']
        commentStr = game.dictionary[game.last]['comment'][idx-1]
        messageBlocks = [SectionBlock(text=message), SectionBlock(text=commentStr) , button_actions]
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(messageBlocks)
    )

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    block_id = click_event.block_id
    block_value = int(click_event.value)

    # 다른 가격대로 다시 크롤링합니다.
    button_actions = ActionsBlock(
        block_id='game_button',
        elements=[
            ButtonElement(
                text="Game Reset",
                action_id="game_reset", value = str(1)
            ),
            ButtonElement(
                text="Game End",
                action_id="game_end", value = str(2)
            ),
        ]
    )
    message = ''
    if block_value == 1:
        #reset
        game.gameEnd()
        game.gameStart()
        message = '*게임이 리셋되었습니다.*'
    else:
        #game end
        game.gameEnd()
        message = '*게임이 종료되었습니다.*'   
        
    messageBlocks = [SectionBlock(text=message), button_actions]
    #message_blocks = []#make_sale_message_blocks(keyword, new_price)

    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(messageBlocks)
    )


    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200



# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)
